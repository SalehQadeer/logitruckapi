﻿using log4net;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;

namespace NSLogger
{
    public class Logger
    {
        public static bool EnableCOMMSLOG = false;
        public static bool EnableDBLOG = false;
        public static bool EnableGENERALLOG = false;
        public static bool EnableTELEPHONYLOG = false;
        public static bool EnableERRORLOG = false;
        public static bool EnableSECURELOG = false;
        public static string LogFileName = string.Empty;
        public static string FilePath = string.Empty;
        public static int LogFileSize = 1024;
        public static int NoOfLogFiles = 5;
        public static string ConfigFilePath = string.Empty;
        public static bool WriteLogsInFile = false;
        public static bool AppedNewLineCharInLogs = false;//sumraiz 24jan2019 in Linux case

        private static ILog logger = log4net.LogManager.GetLogger(typeof(Logger));
        private static XmlDocument log4netConfig = new XmlDocument();

        private static StringBuilder GetMethodAndClassName()
        {
            StringBuilder preamble = null;
            string[] strClassAndMethod = new string[2];
            try
            {

                preamble = new StringBuilder();

                StackTrace stackTrace = new StackTrace();
                StackFrame stackFrame;
                MethodBase stackFrameMethod;

                int frameCount = 0;
                string typeName = string.Empty;
                do
                {
                    frameCount++;
                    stackFrame = stackTrace.GetFrame(frameCount);
                    //Logger.logger.Info("satge1 crossed");//sumraiz for testing

                    stackFrameMethod = stackFrame.GetMethod();
                    //Logger.logger.Info("satge2 crossed :" + stackFrameMethod);//sumraiz for testing

                    typeName = stackFrameMethod.ReflectedType.FullName;

                } while (typeName.StartsWith("System") || typeName.EndsWith("NSLogger.Logger"));

                ////sumraiz 24jan2019 in Linux case commented
                //string strDel = ".";
                //char[] CharArrDelim = strDel.ToCharArray();
                //string[] strBroken = typeName.Split(CharArrDelim, typeName.Length);

                preamble.Append(stackFrameMethod.Name);
            }
            catch (Exception E)
            {
                //logger.Fatal(E.Message, E);//sumraiz 24jan2019 in Linux case commented
                //logger.Fatal(E.Message + E.StackTrace, E);//sumraiz for testing
            }
            return preamble;
        }

        public static void ActivateOptions_Web(string ConfigurationFilePath, ref string Error)
        {
            try
            {
                log4netConfig.Load(File.OpenRead(ConfigFilePath));
                var repo = log4net.LogManager.CreateRepository(
                    Assembly.GetEntryAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));

                //Error += "Repo : " + repo.Name + ", ";
                //Error += "XMl :" + log4netConfig.InnerXml + ", ";

                log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);

                //logger.Info("Application - Main is invoked");
                //logger = LogManager.GetLogger("OmniPCXRecord");
                //  string str = ConfigurationFilePath;
                // log4net.Config.XmlConfigurator.Configure(ConfigurationFilePath, log4netConfig["log4net"]);
                //XmlConfigurator.Configure(new FileInfo(str + @"\Logger.dll.config"));

                createLogPathDirectory(FilePath, ref Error);
                
                //foreach (IAppender appender in LogManager.GetRepository().GetAppenders())

                foreach (log4net.Appender.IAppender appender in log4net.LogManager.GetRepository(Assembly.GetEntryAssembly()).GetAppenders())
                {
                    //if (appender is RollingFileAppender)

                    if (appender is log4net.Appender.RollingFileAppender)
                    {
                        //RollingFileAppender appender2 = (RollingFileAppender)appender;

                        log4net.Appender.RollingFileAppender appender2 = (log4net.Appender.RollingFileAppender)appender;
                        appender2.File = FilePath + @"/" + LogFileName;
                        appender2.MaximumFileSize = Convert.ToString(LogFileSize) + "KB";
                        appender2.MaxSizeRollBackups = NoOfLogFiles;
                        appender2.ActivateOptions();

                        Error += " FilePath:" + appender2.File;
                    }
                }
            }
            catch (Exception E)
            {
                logger.Fatal(E.Message, E);
            }
        }

        public static void WriteLog(string msg, LogLevel LLevel)
        {
            try
            {
                if (!WriteLogsInFile)
                    Console.WriteLine("[" + Logger.GetMethodAndClassName() + "] " + msg);
                else
                {
                    StringBuilder tempSB = new StringBuilder();//sumraiz 24jan2019 in Linux case
                    tempSB.Append("[" + Logger.GetMethodAndClassName() + "] " + msg);

                    if (AppedNewLineCharInLogs)//sumraiz 24jan2019 in Linux case
                        tempSB.Append("\r\n");

                    switch (LLevel)
                    {
                        case LogLevel.COMMSLOG:
                            if (Logger.EnableCOMMSLOG)
                                Logger.logger.Info(tempSB);
                            break;
                        case LogLevel.DBLOG:
                            if (Logger.EnableDBLOG)
                                Logger.logger.Info(tempSB);
                            break;
                        case LogLevel.ERRORLOG:
                            if (Logger.EnableERRORLOG)
                                Logger.logger.Info(tempSB);
                            break;
                        case LogLevel.GENERALLOG:
                            if (Logger.EnableGENERALLOG)
                                Logger.logger.Info(tempSB);
                            break;
                        case LogLevel.TELEPHONYLOG:
                            if (Logger.EnableTELEPHONYLOG)
                                Logger.logger.Info(tempSB);
                            break;
                    }
                }
            }
            catch (Exception exp)
            {
            }
        }

        public static void WriteException(Exception E)
        {
            try
            {
                if (!WriteLogsInFile)
                    Console.WriteLine(E);
                else
                {
                    StringBuilder tempSB = new StringBuilder();
                    tempSB.Append("[" + Logger.GetMethodAndClassName() + "] " + E.Message + E);

                    if (AppedNewLineCharInLogs)//sumraiz 24jan2019 in Linux case
                        tempSB.Append("\r\n");

                    Logger.logger.Info(tempSB);
                }
            }
            catch (Exception ex)
            {
            }
        }

        private static void createLogPathDirectory(string filePath, ref string error)
        {
            try
            {
                if (Directory.Exists(filePath) == false)
                {
                    //NSLogger.Logger.logger.COMMS_INFO("Logging Path Does Not Exist , Creating It", NSLogger.Logger.EnableGENERALLOG );
                    logger.Info("Logging Path Does Not Exist , Creating It");
                    Directory.CreateDirectory(filePath);
                    error += "Directory created: " + filePath;
                }
                else
                {
                    //NSLogger.Logger.logger.COMMS_INFO("Logging Path Exists", NSLogger.Logger.EnableGENERALLOG );
                    logger.Info("Logging Path Exists");
                    error += "Directory already exists: " + filePath;
                }
            }
            catch (Exception E)
            {
                //NSLogger.Logger.logger.COMMS_ERROR(E.Message, E, NSLogger.Logger.EnableERRORLOG);
                logger.Info("[" + GetMethodAndClassName() + "] " + E.Message, E);
            }
        } 
    }

    public enum LogLevel
    {
        COMMSLOG,
        DBLOG,
        GENERALLOG,
        TELEPHONYLOG,
        ERRORLOG,
        SECURELOG
    }
}
