﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;
using LogiTruckAPI.BLL;
using LogiTruckAPI.BO;
using LogiTruckAPI.DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NSLogger;

namespace LogiTruckAPI.Controllers
{
    //[Route("api/[controller]")]
    //[ApiController]
    public class CompanyController : ControllerBase
    {
        [HttpGet]
        public CompanyResponseBO Login(string Phone, string Password)
        {
            CompanyResponseBO responseObj = new CompanyResponseBO();

            try
            {
                if (string.IsNullOrWhiteSpace(Phone) || string.IsNullOrWhiteSpace(Password))
                {
                    FillResponse_CompanyResponseBO(Action.ParamMissing, ref responseObj, System.Reflection.MethodBase.GetCurrentMethod().Name);
                    return responseObj;
                }

                if (clsDatabase.IsDBAvailable(clsDatabase.DBConnectionString))
                {
                    List<DBParameter> Paramters = new List<DBParameter>();

                    DBQuery objDBQuery = null;
                    objDBQuery = new DBQuery(clsDatabase.DBConnectionString, DBCommandType.STOREDPROCEDURE);
                    objDBQuery.CommandText = "sp_getcompanyinfo";

                    Paramters.Add(new DBParameter("phone", Phone, Phone.GetType()));
                    Paramters.Add(new DBParameter("password", Password, Password.GetType()));
                    objDBQuery.Paramteres = Paramters;
                    DataSet dbSet = (DataSet)clsDatabase.ExecuteDataSet(objDBQuery);

                    AppUser userObj = new AppUser();

                    if (dbSet != null)
                    {
                        if (dbSet.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < dbSet.Tables[0].Rows.Count; i++)
                            {
                                userObj.UserType = Convert.ToString(dbSet.Tables[0].Rows[i]["usertype"] + "");
                                userObj.FirstName = Convert.ToString(dbSet.Tables[0].Rows[i]["firstname"] + "");
                                userObj.LastName = Convert.ToString(dbSet.Tables[0].Rows[i]["lastname"] + "");
                                userObj.Phone = Convert.ToString(dbSet.Tables[0].Rows[i]["phone"] + "");
                                userObj.Email = Convert.ToString(dbSet.Tables[0].Rows[i]["email"] + "");
                                userObj.Address = Convert.ToString(dbSet.Tables[0].Rows[i]["address"] + "");
                                userObj.Description = Convert.ToString(dbSet.Tables[0].Rows[i]["description"] + "");
                                userObj.WebsiteUrl = Convert.ToString(dbSet.Tables[0].Rows[i]["websiteurl"] + "");
                                userObj.ServiceAreas = (string[])dbSet.Tables[0].Rows[i]["serviceareas"];
                                userObj.ContractType = Convert.ToString(dbSet.Tables[0].Rows[i]["contracttype"] + "");
                                userObj.Branch = Convert.ToString(dbSet.Tables[0].Rows[i]["branch"] + "");
                            }

                            FillResponse_CompanyResponseBO(Action.OK, ref responseObj, System.Reflection.MethodBase.GetCurrentMethod().Name);
                            responseObj.Company = userObj;
                        }
                        else
                        {
                            FillResponse_CompanyResponseBO(Action.OK, ref responseObj, System.Reflection.MethodBase.GetCurrentMethod().Name);
                        }
                    }
                    else
                    {
                        FillResponse_CompanyResponseBO(Action.InternalServerError, ref responseObj, System.Reflection.MethodBase.GetCurrentMethod().Name);
                    }
                }
                return responseObj;
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex);
                FillResponse_CompanyResponseBO(Action.InternalServerError, ref responseObj, System.Reflection.MethodBase.GetCurrentMethod().Name);
                return responseObj;
            }
        }

        private void FillResponse_DefaultResponseBO(Action ActionPerformed, ref DefaultResponseBO objDefaultResponseBO_Response, string MethodName)
        {
            try
            {
                if (objDefaultResponseBO_Response == null)
                    objDefaultResponseBO_Response = new DefaultResponseBO();

                objDefaultResponseBO_Response = Util.GetResponseBO(ActionPerformed);

                Logger.WriteLog(this.GetType().Name + ":" + MethodName + ":" + objDefaultResponseBO_Response.ToString(), LogLevel.DBLOG);
            }
            catch (Exception exp)
            {
                Logger.WriteException(exp);
            }
        }

        private void FillResponse_CompanyResponseBO(Action ActionPerformed, ref CompanyResponseBO objCompanyBO_Response, string MethodName)
        {
            try
            {
                if (objCompanyBO_Response == null)
                    objCompanyBO_Response = new CompanyResponseBO();

                DefaultResponseBO objTemp_Response = Util.GetResponseBO(ActionPerformed);
                objCompanyBO_Response.ResponseCode = objTemp_Response.ResponseCode;
                objCompanyBO_Response.ResponseDescription = objTemp_Response.ResponseDescription;

                Logger.WriteLog(this.GetType().Name + ":" + MethodName + ":" + objCompanyBO_Response.ToString(), LogLevel.DBLOG);
            }
            catch (Exception exp)
            {
                Logger.WriteException(exp);
            }
        }

    }
}
