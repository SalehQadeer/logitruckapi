using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogiTruckAPI.Controllers;
using LogiTruckAPI.DAL;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NSLogger;

namespace LogiTruckAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            InitializeLogs();
            Logger.WriteLog("Logs activated.", NSLogger.LogLevel.GENERALLOG);

            clsDatabase.DBConnectionString = configuration.GetValue<string>("ConnectionString");
            //CompanyController.PostgresConnStr = configuration.GetValue<string>("ConnectionString");
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            //services.AddMvc(options => options.EnableEndpointRouting = false);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute("Default", "{controller}/{action}");
                //endpoints.MapControllers();
            });
        }

        private static void InitializeLogs()
        {
            try
            {
                Logger.WriteLogsInFile = true;
                Logger.AppedNewLineCharInLogs = false;

                Logger.FilePath = Environment.CurrentDirectory + "/Logs";
                Logger.ConfigFilePath = Environment.CurrentDirectory + "/Logger.dll.config";

                try
                {
                    Logger.NoOfLogFiles = 10;
                    Logger.LogFileSize = 50000;

                    Logger.EnableCOMMSLOG = Logger.EnableTELEPHONYLOG = Logger.EnableDBLOG = Logger.EnableGENERALLOG = Logger.EnableERRORLOG = true;

                    Logger.LogFileName = "LogiTruckAPILogs.txt";
                    string error = string.Empty;
                    Logger.ActivateOptions_Web(Logger.ConfigFilePath, ref error);

                    Logger.WriteLog("FileName:" + Logger.LogFileName + ", Traces Path Set at:" + Logger.FilePath, NSLogger.LogLevel.COMMSLOG);
                }
                catch (Exception ex)
                {
                }
            }
            catch (Exception ex)
            {
            }
        }


    }
}
