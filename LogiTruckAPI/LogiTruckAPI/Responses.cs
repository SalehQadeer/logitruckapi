﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogiTruckAPI
{
    public enum Action//internal Use
    {
        InValidToken,
        DatabaseNotAvailable,
        OK,
        ParamMissing,
        InternalServerError,
        NoPermission,
        CONFLICT,
        TokenExpired
    }
}
