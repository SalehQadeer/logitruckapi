using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Npgsql;
using NSLogger;

namespace LogiTruckAPI.DAL
{
    public class clsDatabase
    {
        private static string _DBConnectionString = string.Empty;

        public static string DBConnectionString
        {
            get
            {
                return _DBConnectionString;
            }
            set
            {
                _DBConnectionString = value;
            }
        }

        public static DatabaseType DatabaseType = DatabaseType.MSSQL;

        public static bool IsDBAvailable(string DBConnectionString)
        {
            try
            {
                return PostgreSQL_IsDBAvailable(DBConnectionString);
            }
            catch (Exception E)
            {
                Logger.WriteException(E);
            }
            return false;
        }

        #region GenericLayer

        #region POSTGRESQL
        public static int PostgreSQL_ExecuteNonQuery(string DBConnectionString, CommandType commandType, string commandText, List<DBParameter> Paramteres)
        {
            int val = -1;
            NpgsqlCommand cmd = new NpgsqlCommand();
            string DBName = PostgreSQL_GetDbName(DBConnectionString);

            Logger.WriteLog("DB:" + DBName + " - SQL query : [ " + commandText + " ]", LogLevel.DBLOG);
            using (NpgsqlConnection conn = new NpgsqlConnection(DBConnectionString))
            {
                try
                {
                    if (conn.State != ConnectionState.Open)
                        conn.Open();

                    cmd.CommandTimeout = 0;
                    PostgreSQL_PrepareCommand(cmd, conn, null, commandType, commandText, Paramteres);

                    DataSet dsReturn = new DataSet();
                    NpgsqlDataAdapter adpt = new NpgsqlDataAdapter();
                    adpt.SelectCommand = cmd;
                    adpt.Fill(dsReturn);

                    //val = Convert.ToInt32(dsReturn.Tables[0].Rows[0][0]);

                    //Logger.WriteLog("DB:" + DBName + " - Rows: [" + val + "] - Query: [" + commandText + "]", LogLevel.DBLOG);
                    cmd.Parameters.Clear();

                    if (conn.State == ConnectionState.Open)
                        conn.Close();

                    return val;
                }
                catch (NpgsqlException E)
                {
                    val = -1;
                    Logger.WriteLog("[Error] DB:" + DBName + " - Query [" + commandText + "]" + " Exception:" + E.Message + E.StackTrace, LogLevel.DBLOG);
                }
                return val;
            }
        }

        public static object PostgreSQL_ExecuteDataSet(string strConnectionString, CommandType commandType, string commandText, List<DBParameter> Paramteres)
        {
            DataSet dsReturn = new DataSet();
            string DBName = PostgreSQL_GetDbName(strConnectionString);
            try
            {
                Logger.WriteLog("DB:" + DBName + " - SQL query : [ " + commandText + " ]", LogLevel.DBLOG);

                NpgsqlCommand cmd = new NpgsqlCommand(commandText);
                NpgsqlDataAdapter adpt = new NpgsqlDataAdapter();

                using (NpgsqlConnection conn = new NpgsqlConnection(strConnectionString))
                {
                    if (conn.State != ConnectionState.Open)
                        conn.Open();

                    cmd.CommandTimeout = 0;
                    PostgreSQL_PrepareCommand(cmd, conn, null, commandType, commandText, Paramteres);

                    adpt.SelectCommand = cmd;
                    adpt.Fill(dsReturn);

                    cmd.Parameters.Clear();

                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                }
                Logger.WriteLog("DB:" + DBName + " - Rows [" + dsReturn.Tables[0].Rows.Count.ToString() + "] - Query [" + commandText + "]", LogLevel.DBLOG);
            }
            catch (Exception E)
            {
                dsReturn = null;
                Logger.WriteLog("[Error] DB: " + DBName + " - Query [" + commandText + "]" + " Exception:" + E.Message + E.StackTrace, LogLevel.DBLOG);
            }
            return dsReturn;
        }

        public static object PostgreSQL_ExecuteDataSetWithoutLogs_NewSite(string strConnectionString, CommandType commandType, string commandText, List<DBParameter> Paramteres)
        {
            DataSet dsReturn = new DataSet();
            string DBName = PostgreSQL_GetDbName(strConnectionString);
            try
            {
                Logger.WriteLog("DB:" + DBName + "", LogLevel.DBLOG);

                NpgsqlCommand cmd = new NpgsqlCommand(commandText);
                NpgsqlDataAdapter adpt = new NpgsqlDataAdapter();

                using (NpgsqlConnection conn = new NpgsqlConnection(strConnectionString))
                {
                    if (conn.State != ConnectionState.Open)
                        conn.Open();

                    cmd.CommandTimeout = 0;
                    PostgreSQL_PrepareCommand(cmd, conn, null, commandType, commandText, Paramteres);

                    adpt.SelectCommand = cmd;
                    adpt.Fill(dsReturn);

                    cmd.Parameters.Clear();

                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                }
                Logger.WriteLog("DB:" + DBName + " - Rows [" + dsReturn.Tables[0].Rows.Count.ToString() + "]", LogLevel.DBLOG);
            }
            catch (Exception E)
            {
                dsReturn = null;
                Logger.WriteLog("[Error] DB: " + DBName + "" + " Exception:" + E.Message + E.StackTrace, LogLevel.DBLOG);
            }
            return dsReturn;
        }

        private static void PostgreSQL_PrepareCommand(NpgsqlCommand cmd, NpgsqlConnection conn, NpgsqlTransaction trans, CommandType cmdType, string cmdText, List<DBParameter> Paramteres)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();

            cmd.Connection = conn;
            cmd.CommandText = cmdText;

            if (trans != null)
                cmd.Transaction = trans;

            cmd.CommandType = cmdType;

            if (Paramteres != null)
            {
                foreach (DBParameter parm in Paramteres)
                    cmd.Parameters.AddWithValue("v_" + parm.Name.ToLower(), Convert.ChangeType(parm.Value, parm.Type));
            }
        }

        private static string PostgreSQL_GetDbName(string PostgreSQLConString)
        {
            NpgsqlConnectionStringBuilder objSqlConnectionStringBuilder = new NpgsqlConnectionStringBuilder(PostgreSQLConString);
            return objSqlConnectionStringBuilder.Database;
        }

        private static string PostgreSQL_GetUpdatedConStr(string NewDBName, string PostgreSQLConString)
        {
            NpgsqlConnectionStringBuilder objSqlConnectionStringBuilder = new NpgsqlConnectionStringBuilder(PostgreSQLConString);
            objSqlConnectionStringBuilder.Database = NewDBName.ToLower();

            string ConnectionString = objSqlConnectionStringBuilder.ConnectionString;

            if (ConnectionString.StartsWith("Host"))
                ConnectionString = "server" + ConnectionString.Substring(4);
            if (ConnectionString.Contains("Username="))
                ConnectionString = ConnectionString.Replace("Username=", "User id=");

            return ConnectionString;
        }

        private static bool PostgreSQL_IsDBAvailable(string DBConnectionString)
        {
            bool bReturn = false;
            try
            {
                NpgsqlConnection conn = new NpgsqlConnection(DBConnectionString);

                if (conn.State != ConnectionState.Open)
                    conn.Open();

                bReturn = true;

                try
                {
                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                }
                catch (Exception ex) { }
            }
            catch (Exception E)
            {
                Logger.WriteException(E);
            }
            return bReturn;
        }
        #endregion

        public static string GetDbName(string ConString)
        {
            try
            {
                NpgsqlConnectionStringBuilder objSqlConnectionStringBuilder = new NpgsqlConnectionStringBuilder(ConString);
                return objSqlConnectionStringBuilder.Database;
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex);
                throw ex;
            }
            return string.Empty;
        }

        public static string GetUpdatedConStr(string NewDBName, string ConString)
        {
            try
            {
                return PostgreSQL_GetUpdatedConStr(NewDBName, ConString);
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex);
                throw ex;
            }
            return string.Empty;
        }

        public static int ExecuteNonQuery(DBQuery DBQuery)
        {
            try
            {
                return PostgreSQL_ExecuteNonQuery(DBQuery.DBConnectionString, (DBQuery.DBCommandType == 0) ? CommandType.StoredProcedure : CommandType.Text, DBQuery.CommandText, DBQuery.Paramteres);
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex);
                throw ex;
            }
            return -1;
        }

        public static object ExecuteDataSet(DBQuery DBQuery)
        {
            try
            {
                return PostgreSQL_ExecuteDataSet(DBQuery.DBConnectionString, (DBQuery.DBCommandType == 0) ? CommandType.StoredProcedure : CommandType.Text, DBQuery.CommandText, DBQuery.Paramteres);
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex);
                throw ex;
            }
            return null;
        }

        public static object ExecuteDataSetWithOutLogs_NewSite(DBQuery DBQuery)
        {
            try
            {
                return PostgreSQL_ExecuteDataSetWithoutLogs_NewSite(DBQuery.DBConnectionString, (DBQuery.DBCommandType == 0) ? CommandType.StoredProcedure : CommandType.Text, DBQuery.CommandText, DBQuery.Paramteres);
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex);
                throw ex;
            }
            return null;
        }
        #endregion
    }

    public enum DatabaseType
    {
        MSSQL = 0,
        POSTGRESQL = 1
    }
    public enum DBCommandType
    {
        STOREDPROCEDURE = 0,
        TEXT = 1
    }
    public class DBQuery
    {
        public DBQuery(string DBConnectionString, DBCommandType DBCommandType)
        {
            this.DBConnectionString = DBConnectionString;
            this.DBCommandType = DBCommandType;
        }

        //public string LogIdentifier = string.Empty;
        public string DBConnectionString = string.Empty;
        public DBCommandType DBCommandType = DBCommandType.STOREDPROCEDURE;

        public string CommandText { get; set; }
        public List<DBParameter> Paramteres { get; set; }
    }

    public class DBParameter
    {
        public DBParameter(string Name, object Value, Type Type)
        {
            this.Name = Name;
            this.Value = Value;
            this.Type = Type;
        }
        public string Name { get; set; }
        public object Value { get; set; }
        public Type Type { get; set; }
    }
}