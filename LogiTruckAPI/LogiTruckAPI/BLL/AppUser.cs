﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogiTruckAPI.BLL
{
    public class AppUser
    {
        public string UserType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string WebsiteUrl { get; set; }
        public string[] ServiceAreas { get; set; }
        public string ContractType { get; set; }
        public string Branch { get; set; }
    }
}
