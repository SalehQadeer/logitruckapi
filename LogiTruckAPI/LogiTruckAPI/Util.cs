﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogiTruckAPI.Controllers;
using LogiTruckAPI.BO;
using System.Security.Cryptography;
using System.IO;
using NSLogger;

namespace LogiTruckAPI
{
    public class Util
    {
        public static DefaultResponseBO GetResponseBO(Action ActionPerformed)
        {
            DefaultResponseBO objDefaultResponseBO = new DefaultResponseBO();
            try
            {
                if (ActionPerformed == Action.InValidToken)
                {
                    objDefaultResponseBO.ResponseCode = 401;
                    objDefaultResponseBO.ResponseDescription = "UNAUTHORIZED";
                }
                else if (ActionPerformed == Action.DatabaseNotAvailable || ActionPerformed == Action.InternalServerError)
                {
                    objDefaultResponseBO.ResponseCode = 500;
                    objDefaultResponseBO.ResponseDescription = "INTERNAL SERVER ERROR";
                }
                else if (ActionPerformed == Action.ParamMissing)
                {
                    objDefaultResponseBO.ResponseCode = 400;
                    objDefaultResponseBO.ResponseDescription = "BAD REQUEST";
                }
                else if (ActionPerformed == Action.NoPermission)
                {
                    objDefaultResponseBO.ResponseCode = 403;
                    objDefaultResponseBO.ResponseDescription = "FORBIDDEN";
                }
                else if (ActionPerformed == Action.CONFLICT)
                {
                    objDefaultResponseBO.ResponseCode = 409;
                    objDefaultResponseBO.ResponseDescription = "CONFLICT";
                }
                else if (ActionPerformed == Action.TokenExpired)
                {
                    objDefaultResponseBO.ResponseCode = 440;
                    objDefaultResponseBO.ResponseDescription = "TOKENEXPIRED";
                }
                else
                {
                    objDefaultResponseBO.ResponseCode = 200;
                    objDefaultResponseBO.ResponseDescription = "OK";
                }
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex);
            }
            return objDefaultResponseBO;
        }

    }
}
