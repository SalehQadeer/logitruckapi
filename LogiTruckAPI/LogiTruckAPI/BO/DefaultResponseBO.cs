﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogiTruckAPI.BO
{
    public class DefaultResponseBO
    {
        public int ResponseCode { get; set; }
        public string ResponseDescription { get; set; }

        public override string ToString()
        {
            return "DefaultResponseBO - " + ResponseCode + ":" + ResponseDescription;
        }
    }
}
